const { sequelize, User, Address, Adhar, Role } = require("./models");
const express = require("express");
const user = require("./models/user");
const { use } = require("express/lib/application");
const role = require("./models/role");

const app = express();
app.use(express.json());

app.post("/users", async (req, res) => {
  const { name, email, number } = req.body;
  try {
    const users = await User.create({ name, email, number });
    return res.json(users);
  } catch (error) {
    console.log(error);
    return res.status(500).json(error);
  }
});

app.get("/users", async (req, res) => {
  try {
    const users = await User.findAll({ include: [Address, Adhar, Role] });
    console.log(users);
    return res.json(users);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: "Something went wrong" });
  }
});

app.get("/users/:uuid", async (req, res) => {
  const uuid = req.params.uuid;
  try {
    const users = await User.findOne({
      where: { uuid },
    });
    return res.json(users);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: "Something Went wrong" });
  }
});

app.delete("/users/:uuid", async (req, res) => {
  const uuid = req.params.uuid;
  try {
    const users = await User.findOne({
      where: { uuid },
    });
    await users.destroy();
    return res.json({ message: "User Deleted" });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: "Something Went wrong" });
  }
});

app.put("/users/:uuid", async (req, res) => {
  const uuid = req.params.uuid;
  const { name, email, number } = req.body;
  try {
    const users = await User.findOne({
      where: { uuid },
    });
    users.name = name;
    users.email = email;
    users.number = number;
    await users.save();

    return res.json(users);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: "Something Went wrong" });
  }
});

// app.post ('/users/:uuid/address', async (req, res) => {
//     const uuid = req.params.uuid;
//     // return res.json({uuid: uuid+"Id of an user"})
//     // const {addr} = req.body;
//     try {
//         const user = await User.findOne({ where: {uuid: uuid}});
//         // const post = await Address.create ({ addr, userId: user.id });
//         const address = await user.createAddress(req.body);
//         return res.json(address);
//     }catch(error) {
//         console.log(error);
//         return res.status(500).json(error);
//     }
// })

app.post("/users/:uuid/address", async (req, res) => {
  const uuid = req.params.uuid;
  const addr = req.body;
  try {
    const users = await User.findOne({
      where: {
        uuid: uuid,
      },
    });
    const address = await users.createAddress(req.body);

    return res.json(address);
  } catch (error) {
    console.log(error);
    return res.status(500).json(error);
  }
});

// app.get ('/address', async (req, res) => {
//     try {
//         const addr = await Address.findAll( { include: [User]});
//         return res.json(addr);
//     } catch (error) {
//         console.log(error);
//         return res.status(500).json(error);
//     }
// })

app.get("/users/:uuid/address", async (req, res) => {
  // const uuid = req.params.uuid;
  try {
    const addr = await Address.findAll();
    return res.json(addr);
  } catch (error) {
    console.log(error);
    return res.status(500).json(error);
  }
});

app.get("/users/:uuid/address/:uuid", async (req, res) => {
  const uuid = req.params.uuid;
  try {
    const addr = await Address.findOne({
      where: { uuid },
    });
    return res.json(addr);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: "Something Went wrong" });
  }
});

app.put("/users/:uuid/address/:uuid", async (req, res) => {
  const uuid = req.params.uuid;
  const { addr } = req.body;
  try {
    const address = await Address.findOne({
      where: { uuid },
    });
    address.addr = addr;
    await address.save();
    return res.json(address);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: "Something Went wrong" });
  }
});

app.post("/users/:uuid/aadhar", async (req, res) => {
  const uuid = req.params.uuid;
  const { aadhar } = req.body;
  try {
    const user = await User.findOne({ where: { uuid: uuid }});
    const adharDetails = await Adhar.create({ aadhar, userId: user.id });
    // const address = await user.createAddress(req.body);
    return res.json(adharDetails);
  } catch (error) {
    console.log(error);
    return res.status(500).json(error);
  }
});


app.get ('/users/:uuid/aadhar', async (req, res) => {
    const uuid = req.params.uuid;
    // const {aadhar} = req.body;
    try {
        const user = await User.findOne({
            where: {
                uuid
            }, include: [Adhar]
        });
        // const adharDetails = await Adhar.create ({ aadhar, userId: user.id });
        // const address = await user.createAddress(req.body);
        return res.json(user);
    }catch(error) {
        console.log(error);
        return res.status(500).json(error);
    }
})


app.post("/users/:uuid/role", async (req, res) => {
    const uuid = req.params.uuid;
    const addr = req.body;
    try {
      const users = await User.findOne({
        where: {
          uuid: uuid,
        },
      });
      const roles = await users.createRole(req.body);
  
      return res.json(roles);
    } catch (error) {
      console.log(error);
      return res.status(500).json(error);
    }
  });

  app.get ('/users/:uuid/role', async (req, res) => {
      const uuid = req.params.uuid;
      try {
        //   const users = await User.findOne({
        //       where: {
        //           uuid: uuid
        //       }
        //   })
          const role = await Role.findAll();
          return res.json(role);
      } catch (error) {
          console.log(error);
          return res.status(500).json(error);
      }
  })

  app.get ('/users/:uuid/role/:uuid', async (req, res) => {
    const uuid = req.params.uuid;
    try {
        const role = await Role.findOne({
            where: {
                uuid: uuid,
            }
        })
        return res.json(role);
    } catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }
})

app.delete ('/users/:uuid/role/:uuid', async (req, res) => {
    const uuid = req.params.uuid;
    try {
        const role = await Role.findOne({
            where: {
                uuid: uuid,
            }
        })
        await role.destroy();
        return res.json({"role" : "Role is deleted"});
    } catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }
})

app.put ('/users/:uuid/role/:uuid', async (req, res) => {
    const uuid = req.params.uuid;
    const {role} = req.body;
    try {
        const roles = await Role.findOne({
            where: {
                uuid: uuid,
            }
        })
        roles.role = role;
        await roles.save();
        return res.json(roles);
    } catch (error) {
        console.log(error);
        return res.status(500).json(error);
    }
})



// app.get('/address/:uuid', async (req, res) => {
//     const uuid = req.params.uuid;
//     try {
//         const addr = await Address.findOne({
//             where: {uuid},
//         })
//         return res.json(addr);
//     } catch (error) {
//         console.log(error);
//         return res.status(500).json({ error: "Something Went wrong"})
//     }
// })

app.listen({ port: 5000 }, async () => {
  console.log("server up on http://localhost:5000");
  await sequelize.authenticate();
  console.log("Database Connected");
});
