"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    await queryInterface.bulkInsert(
      "users",
      [
        {
          name: "John Doe",
          email: "john@gmail.com",
          number: "8597803526",
          uuid: "7b139389-1ba4-400b-9055-81f47526f0cb",
          createdAt: "2022-02-03T17:19:40.389Z",
          updatedAt: "2022-02-03T18:26:23.055Z",
        },
        {
          name: "Tom",
          email: "tom@gmail.com",
          number: "6245034533",
          uuid: "7b139389-1ba4-400b-9055-81f47526f0cb",
          createdAt: "2022-02-03T17:19:40.389Z",
          updatedAt: "2022-02-03T18:26:23.055Z",
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("users", null, {});
  },
};
