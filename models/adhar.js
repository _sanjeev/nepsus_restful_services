'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Adhar extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({User}) {
      // define association here
      //userId
      this.belongsTo(User, {foreignKey: "userId"});
    }
    toJSON() {
      return {...this.get(), id:undefined, userId:undefined}
    }
  }
  Adhar.init({
    uuid: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    aadhar: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'adhar',
    modelName: 'Adhar',
  });
  return Adhar;
};