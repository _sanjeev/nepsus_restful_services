'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Address, Adhar, Role }) {
      // define association here
      this.hasMany ( Address, {foreignKey : 'userId'});
      this.hasOne (Adhar, {foreignKey: 'userId'});
      this.hasMany(Role, {foreignKey: 'userId'});
    }
    toJSON() {
      return {...this.get(), id: undefined}
    }
  }
  User.init({
    uuid: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {msg: 'User must have a name'},
        notEmpty: {msg: "Name must not be an empty"}
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {msg: 'User must have a Email'},
        notEmpty: {msg: "Email must not be an empty"},
        isEmail: {msg: "Must be a valid email address"}
      }
    },
    number: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {msg: 'User must have a number'},
        notEmpty: {msg: "Number must not be an empty"}
      }
    }
  }, {
    sequelize,
    tableName: 'users',
    modelName: 'User',
  });
  return User;
};